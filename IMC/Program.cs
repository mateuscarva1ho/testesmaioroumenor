﻿using System;

namespace IMC
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario Usuario = new Usuario();
            CalculoIMC Funcoes = new CalculoIMC();

            Console.WriteLine("digite seu nome ");
            Usuario.Nome = Console.ReadLine();

            Console.WriteLine("digite seu Email ");
            Usuario.Email = Console.ReadLine();

            Console.WriteLine("digite seu telefone ");
            Usuario.Telefone = Console.ReadLine();

            Console.WriteLine("digite seu idade ");
            Usuario.Idade = Console.ReadLine();

            Console.WriteLine("digite seu Peso ");
            Usuario.Peso = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("digite sua Altura ");
            Usuario.Altura = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("\n");

            double valorImc = Funcoes.CalcularImc(Usuario);
            string classificacao = Funcoes.ClassificarIMC(valorImc);

            Console.WriteLine(valorImc);

            Console.WriteLine("\n");

            Console.WriteLine(classificacao);

        }
        
    }
}
