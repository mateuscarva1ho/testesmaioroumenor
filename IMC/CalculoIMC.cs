﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMC
{
    class CalculoIMC
    {

        public double CalcularImc(Usuario usuario)
        {

            double imc = usuario.Peso / (usuario.Altura * usuario.Altura);
            return imc;
        }
        public string ClassificarIMC(double imc)
        {
            if (imc < 18.5)
            {
                return "Baixo Peso";
            }
            else
            {
                if (imc >= 18.5 && imc <= 24.9)
                {
                    return "Peso normal";
                }
                else
                {
                    if (imc >= 25 && imc <= 29.9)
                    {
                        return "Pré-obeso";
                    }
                    else
                    {
                        if (imc >= 30 && imc <= 34.9)
                        {
                            return "Obeso I";
                        }
                        else
                        {
                            if (imc >= 35 && imc <= 39.9)
                            {
                                return "Obeso II";
                            }
                            else
                            {
                                return "Obeso III";
                            }
                        }
                    }
                    
                }
            }
        }
    }
}
    

        

    

