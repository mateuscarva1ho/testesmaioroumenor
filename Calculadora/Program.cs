﻿using System;
using Calculadora;

namespace Calculadora
{
    class Program
    {
        static void Main(string[] args)
        {
            Calcular calcular = new Calcular();

            Console.WriteLine("Digite o primeiro valor: ");
            calcular.numero1 = double.Parse(Console.ReadLine());

            Console.WriteLine("Digite o segundo valor: ");
            calcular.numero2 = double.Parse(Console.ReadLine());

            Console.WriteLine("Digite o tipo de operação");
            calcular.operacoes = Console.ReadLine();


            double valor2 = calcular.Valor();

            Console.WriteLine("Total: ");
            Console.WriteLine(valor2);

        }
    }
}
